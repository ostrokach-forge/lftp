#!/bin/bash

export PKG_CONFIG_PATH="${PREFIX}/share/pkgconfig:${PREFIX}/lib/pkgconfig"
export PKG_CONFIG_LIB="${PREFIX}/lib/pkgconfig"

export CFLAGS="-w -I$PREFIX/include"
export CPPFLAGS="$CFLAGS"
export CXXFLAGS="$CFLAGS"

export LDFLAGS="-L$PREFIX/lib"

./configure \
    --prefix="${PREFIX}" \
    --with-readline="${PREFIX}" \
    --with-openssl="${PREFIX}" \
    --with-zlib="${PREFIX}" \
    --with-expat="${PREFIX}"
make
make install

